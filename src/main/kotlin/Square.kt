import java.util.*

class Square {
      var scanner = Scanner(System.`in`)
      var  sideLength : Int = scanner.nextInt()
    fun displaySideLength() {

        println("Edge length: " + sideLength)
    }

    fun calculateAndDisplayPerimeter() {
        val perimeter: Int = 4 * sideLength
        println("Square perimeter: $perimeter")
    }

    fun calculateAndDisplayArea() {
        val area: Int = sideLength * sideLength
        println("Area square: $area")
    }

    fun displayAllInfo() {
        displaySideLength()
        calculateAndDisplayPerimeter()
        calculateAndDisplayArea()
    }

}
