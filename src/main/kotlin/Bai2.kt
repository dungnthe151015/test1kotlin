import java.util.*

class Bai2 {
    fun bai2(){
        val scanner = Scanner(System.`in`)
        println("Enter the amount of electricity consumed in the month (kWh): ")
        var amountOfElectricityConsumed : Int =scanner.nextInt()
        var electricityBill : Int = 0

        electricityBill = if (amountOfElectricityConsumed <= 25) {
            amountOfElectricityConsumed * 1000
        } else if (amountOfElectricityConsumed > 25 && amountOfElectricityConsumed <= 75) {
            25 * 1000 + (amountOfElectricityConsumed - 25) * 1250
        } else if (amountOfElectricityConsumed > 75 && amountOfElectricityConsumed <= 150) {
            25 * 1000 + 50 * 1250 + (amountOfElectricityConsumed - 75) * 1800
        } else {
            25 * 1000 + 50 * 1250 + 75 * 1800 + (amountOfElectricityConsumed - 150) * 2500
        }

        println("The amount of electricity to be paid for the month is: $electricityBill")
    }
}