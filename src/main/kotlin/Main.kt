import java.util.Scanner


fun main(args: Array<String>) {
    val scanner = Scanner(System.`in`)
    var controller = Controller()
    while (true) {
        //display menu
        displayMenu()
        //select option
        selectOption()
        val option: Int = scanner.nextInt()
        when (option) {
            1 -> controller.bai1()
            2 -> controller.bai2()
            3 -> controller.bai3()
            4 -> System.exit(0)
            else -> {
                // Handle any other cases if needed
                println("Invalid choice")
            }
        }
    }
}

fun displayMenu() {
    println("Enter your option: \", \"Option[1-4]")
}

fun selectOption() {
    println("1. Bai1")
    println("2. Bai2")
    println("3. Bai3")
    println("4. Exit")
}