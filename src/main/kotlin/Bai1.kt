import java.util.*

class Bai1 {
    fun bai1() {
        val scanner = Scanner(System.`in`)

        println("Enter number a: ")
        val a : Double = scanner.nextDouble()
        println("Enter number b: ")
        val b : Double = scanner.nextDouble()
        println("Enter number c: ")
        val c : Double = scanner.nextDouble()

        val delta : Double = b * b - 4 * a * c

        if(delta <0){
            println("Delta is a negative number, delta roots cannot be calculated in the set of real numbers.")
        } else {
            val sqrtDelta : Double = Math.sqrt(delta)
            println("delta = " + sqrtDelta)
        }
    }
}